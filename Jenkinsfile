pipeline {
    agent {
        docker {
            image 'docker:20.10.16'
            args '-u root --privileged -v /var/run/docker.sock:/var/run/docker.sock'
        }
    }
    environment {
        GIT_REPO_URL = 'https://gitlab.com/MuyleangIng1/autograde.git'
        GIT_BRANCH = 'main'
    }
    stages {
        stage('Git Clone') {
            steps {
                script {
                    // Clone the repository
                    checkout([$class: 'GitSCM', branches: [[name: "*/${GIT_BRANCH}"]], userRemoteConfigs: [[url: "${GIT_REPO_URL}"]]])
                }
            }
        }
        stage('Build') {
            agent {
                docker {
                    image 'maven:3.9.6-eclipse-temurin-21-alpine'
                    args '-u root --privileged -v /var/run/docker.sock:/var/run/docker.sock'
                }
            }
            steps {
                sh 'mvn -V --color always -ntp -fae test-compile'
            }
            post {
                always {
                    archiveArtifacts artifacts: 'target/**/*', allowEmptyArchive: true
                    echo 'Build stage completed.'
                }
                failure {
                    error 'Build stage failed.'
                }
            }
        }
        stage('Analysis') {
            agent {
                docker {
                    image 'maven:3.9.6-eclipse-temurin-21-alpine'
                    args '-u root --privileged -v /var/run/docker.sock:/var/run/docker.sock'
                }
            }
            steps {
                sh 'mvn -V --color always -ntp -X verify -DskipTests -DskipITs'
            }
            post {
                always {
                    archiveArtifacts artifacts: 'target/**/*', allowEmptyArchive: true
                    echo 'Analysis stage completed.'
                }
                failure {
                    error 'Analysis stage failed.'
                }
            }
        }
        stage('Test') {
            agent {
                docker {
                    image 'maven:3.9.6-eclipse-temurin-21-alpine'
                    args '-u root --privileged -v /var/run/docker.sock:/var/run/docker.sock'
                }
            }
            steps {
                sh 'mvn -V --color always -ntp -fae verify'
            }
            post {
                always {
                    archiveArtifacts artifacts: 'target/**/*', allowEmptyArchive: true
                    echo 'Test stage completed.'
                }
                failure {
                    error 'Test stage failed.'
                }
            }
        }
        stage('PITest') {
            agent {
                docker {
                    image 'maven:3.9.6-eclipse-temurin-21-alpine'
                    args '-u root --privileged -v /var/run/docker.sock:/var/run/docker.sock'
                }
            }
            steps {
                sh 'mvn -V --color always -ntp org.pitest:pitest-maven:mutationCoverage'
            }
            post {
                always {
                    archiveArtifacts artifacts: 'target/**/*', allowEmptyArchive: true
                    echo 'PIT test stage completed.'
                }
                failure {
                    error 'PIT test stage failed.'
                }
            }
        }
        stage('Autograding') {
            agent {
                docker {
                    image 'uhafner/autograding-gitlab-action:v1'
                    args '-u root --privileged -v /var/run/docker.sock:/var/run/docker.sock'
                }
            }
            environment {
                MAX_WARNING_COMMENTS = '10'
                MAX_COVERAGE_COMMENTS = '10'
                CONFIG = '''
                {
                    "tests": {
                        "name": "Tests",
                        "tools": [
                            {
                                "id": "test",
                                "name": "Tests",
                                "pattern": "**/target/*-reports/TEST*.xml"
                            }
                        ],
                        "passedImpact": 0,
                        "skippedImpact": -1,
                        "failureImpact": -5,
                        "maxScore": 100
                    },
                    "analysis": [
                        {
                            "name": "Style",
                            "id": "style",
                            "tools": [
                                {
                                    "id": "checkstyle",
                                    "name": "CheckStyle",
                                    "pattern": "**/target/checkstyle-*/checkstyle-result.xml"
                                },
                                {
                                    "id": "pmd",
                                    "name": "PMD",
                                    "pattern": "**/target/pmd-*/pmd.xml"
                                }
                            ],
                            "errorImpact": -1,
                            "highImpact": -1,
                            "normalImpact": -1,
                            "lowImpact": -1,
                            "maxScore": 100
                        },
                        {
                            "name": "Bugs",
                            "id": "bugs",
                            "icon": "bug",
                            "tools": [
                                {
                                    "id": "spotbugs",
                                    "name": "SpotBugs",
                                    "sourcePath": "src/main/java",
                                    "pattern": "**/target/spotbugsXml.xml"
                                }
                            ],
                            "errorImpact": -3,
                            "highImpact": -3,
                            "normalImpact": -3,
                            "lowImpact": -3,
                            "maxScore": 100
                        }
                    ],
                    "coverage": [
                        {
                            "name": "Code Coverage",
                            "tools": [
                                {
                                    "id": "jacoco",
                                    "name": "Line Coverage",
                                    "metric": "line",
                                    "sourcePath": "src/main/java",
                                    "pattern": "**/target/site/jacoco/jacoco.xml"
                                },
                                {
                                    "id": "jacoco",
                                    "name": "Branch Coverage",
                                    "metric": "branch",
                                    "sourcePath": "src/main/java",
                                    "pattern": "**/target/site/jacoco/jacoco.xml"
                                }
                            ],
                            "maxScore": 100,
                            "missedPercentageImpact": -1
                        },
                        {
                            "name": "Mutation Coverage",
                            "tools": [
                                {
                                    "id": "pit",
                                    "name": "Mutation Coverage",
                                    "metric": "mutation",
                                    "sourcePath": "src/main/java",
                                    "pattern": "**/target/pit-reports/mutations.xml"
                                }
                            ],
                            "maxScore": 100,
                            "missedPercentageImpact": -1
                        }
                    ]
                }
                '''
            }
            steps {
                script {
                    sh 'java -cp @/app/jib-classpath-file edu.hm.hafner.grading.gitlab.GitLabAutoGradingRunner'
                }
            }
            post {
                always {
                    echo 'Autograding stage completed.'
                }
                failure {
                    error 'Autograding stage failed.'
                }
            }
        }
    }
    post {
        always {
            cleanWs()
        }
    }
}
